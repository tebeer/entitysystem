﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFES;

public class InputHandler : MonoBehaviour
{
    public Material uiMaterial;
    private Test2.GameEntitySystem m_system;

    public void Init(Test2.GameEntitySystem system)
    {
        m_system = system;
        m_processor = new Processor<Transform, Controllable>(m_system);
    }

    void OnPostRender()
    {
        if (m_drag)
        {
            GL.PushMatrix();
            uiMaterial.SetPass(0);
            GL.LoadPixelMatrix();

            GL.Color(Color.red);

            GL.Begin(GL.LINE_STRIP);
            GL.Vertex3(m_dragRect.xMin, m_dragRect.yMax, 0);
            GL.Vertex3(m_dragRect.xMax, m_dragRect.yMax, 0);
            GL.Vertex3(m_dragRect.xMax, m_dragRect.yMin, 0);
            GL.Vertex3(m_dragRect.xMin, m_dragRect.yMin, 0);
            GL.Vertex3(m_dragRect.xMin, m_dragRect.yMax, 0);
            GL.End();

            GL.Begin(GL.TRIANGLE_STRIP);
            GL.Vertex3(m_dragRect.xMin, m_dragRect.yMax, 0);
            GL.Vertex3(m_dragRect.xMax, m_dragRect.yMax, 0);
            GL.Vertex3(m_dragRect.xMin, m_dragRect.yMin, 0);
            GL.Vertex3(m_dragRect.xMax, m_dragRect.yMin, 0);
            GL.End();

            GL.PopMatrix();
        }
    }

	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_touchStart = Input.mousePosition;
            m_drag = true;
        }

        if (Input.GetMouseButton(0))
        {
            m_touchEnd = Input.mousePosition;
            UpdateDragRect();
        }

        if (Input.GetMouseButtonUp(0))
        {
            m_drag = false;
            m_touchEnd = Input.mousePosition;
            UpdateDragRect();

            m_newSelected.Clear();

            m_processor.Process(TrySelect);

            var screenRay = Camera.main.ScreenPointToRay(m_touchEnd);
            RaycastHit hitInfo;

            if (m_newSelected.Count == 0 && m_selected.Count > 0 
                && Physics.Raycast(screenRay, out hitInfo, 1000, 1 << LayerMask.NameToLayer("Ground")))
            {
                int worldX = FixMath.FromFloat(hitInfo.point.x);
                int worldY = FixMath.FromFloat(hitInfo.point.z);

                for (int i = 0; i < m_selected.Count; ++i)
                {
                    if (!m_system.EntityExists(m_selected[i]))
                        continue;

                    if (!m_system.HasComponent<MoveTarget>(m_selected[i]))
                        continue;

                    MoveTarget target = new MoveTarget();
                    target.x = worldX;
                    target.y = worldY;

                    m_system.SetComponent<MoveTarget>(m_selected[i], target);
                }
            }
            else
                m_selected = new List<int>(m_newSelected);
            //Debug.Log(m_selected.Count);
            //var transforms = system.GetComponentStorage<Transform>();
            //for (int i = 0; i < transforms.Count; ++i)
            //{
            //    var t = transforms.m_components[i];
            //}
        }
	}

    private void TrySelect(int i, ref Transform t, ref Controllable c)
    {
        //m_entityObjects[entityID].position = new Vector3(FixMath.ToFloat(transform.x), 0, FixMath.ToFloat(transform.y));
        var worldPos = new Vector3(FixMath.ToFloat(t.x), 0, FixMath.ToFloat(t.y));
        var screenPos = Camera.main.WorldToScreenPoint(worldPos);

        if(m_dragRect.Contains(screenPos))
        {
            m_newSelected.Add(i);
        }
    }

    private void UpdateDragRect()
    {
        if (m_touchEnd.x < m_touchStart.x)
        {
            m_dragRect.x = m_touchEnd.x;
            m_dragRect.width = m_touchStart.x - m_touchEnd.x;
        }
        else
        {
            m_dragRect.x = m_touchStart.x;
            m_dragRect.width = m_touchEnd.x - m_touchStart.x;
        }

        if (m_touchEnd.y < m_touchStart.y)
        {
            m_dragRect.y = m_touchEnd.y;
            m_dragRect.height = m_touchStart.y - m_touchEnd.y;
        }
        else
        {
            m_dragRect.y = m_touchStart.y;
            m_dragRect.height = m_touchEnd.y - m_touchStart.y;
        }
    }

    private Vector2 m_touchStart;
    private Vector2 m_touchEnd;
    private bool m_drag;
    private Rect m_dragRect;

    private List<int> m_selected = new List<int>();
    private List<int> m_newSelected = new List<int>();
    private Processor<Transform, Controllable> m_processor;
}
