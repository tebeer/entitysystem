﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Stopwatch = System.Diagnostics.Stopwatch;

using Test2;

public class World : MonoBehaviour
{
    public GameObject[] graphics;

    private CFES.Processor<TeamData> m_teamDataProcessor;

	void Start ()
    {
        m_system = new GameEntitySystem();

        FindObjectOfType<InputHandler>().Init(m_system);

        new GameManager(m_system);

        var graphics = new GraphicsProcessor(m_system);
        //var graphics = new GraphicsProcessor();
        //graphics.Register(m_system);
        graphics.world = this;

        m_system.Start();

        string str = "";
        for (int i = 0; i < 360; ++i)
        {
            str += (int)( Mathf.Tan(Mathf.Deg2Rad * i)) + ",";
        }
        //Debug.Log(str);

        m_teamDataProcessor = new CFES.Processor<TeamData>(m_system);
	}

    void OnGUI()
    {
        GUILayout.Label(m_updateMS.ToString());
        //GUILayout.Label(FixMath.FromFloat(Time.fixedDeltaTime).ToString());
        //GUILayout.Label("Sort 1: " + m_sortTime1.ToString());        
        //GUILayout.Label(m_system.Team[m_system.EntityPtr[ptr]].teamID.ToString());        

        foreach (var array in m_system.GetComponentStorages())
        {
            GUILayout.Label(array.GetType().ToString() + " " + array.Count);        
        }

        m_teamDataProcessor.Process(ShowTeamData);
    }

    void ShowTeamData(int entityID, ref TeamData teamData)
    {
        //Debug.Log("ShowTeamData" + entityID);
        GUILayout.Label("Team " + entityID);
        GUILayout.Label(string.Format("Energy: {0}/{1}", teamData.energy, teamData.maxEnergy));
        GUILayout.Label(string.Format("Metal: {0}/{1}", teamData.metal, teamData.maxMetal));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            sw.Reset();
            sw.Start();
            //m_system.InsertionSort();
            //for(int i = 0; i < m_system.LiveEntityCount; ++i)
            //    m_system.Sort(i);
            m_sortTime1 = GetMs(sw);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            string str = "";
            //for (int i = 0; i < m_system.EntityCount; ++i)
            //{
            //    string b = "";
            //    for (int j = 0; j < 31; ++j)
            //    {
            //        if((m_system.m_mask[i] & (1 << j)) != 0)
            //            b += "1";
            //        else
            //            b += "0";
            //    }
            //    str += b + " " + m_system.m_entityPtr[i] + " " + m_system.m_entityIndex[i] + "\n";
            //}
            Debug.Log(str);
        }
    }

    double GetMs(Stopwatch sw)
    {
        return (double)sw.ElapsedTicks * 1000 / Stopwatch.Frequency;
    }

	void FixedUpdate ()
    {
        sw.Reset();
        sw.Start();
        m_system.Update(FixMath.FromFloat(Time.fixedDeltaTime));
        var ticks = sw.ElapsedTicks;
        m_updateMS = GetMs(sw);

        //string str = "";
        //for (int i = 0; i < m_system.EntityCount; ++i)
        //    str += m_system.Entity.Mask[i] + " ";
        //Debug.Log(str);
	}

    private double m_sortTime1;
    private double m_updateMS;
    private Stopwatch sw = new Stopwatch();
    private GameEntitySystem m_system;
}
