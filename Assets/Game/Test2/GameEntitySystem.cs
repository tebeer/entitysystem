﻿using UnityEngine;
using System.Collections;

namespace Test2
{
    public class GameEntitySystem : CFES.ComponentEntitySystem
    {
        public GameEntitySystem()
            : base()
        {
            //RegisterComponentArray<Transform>();
            //RegisterComponentArray<Velocity>();
            //RegisterComponentArray<Graphic>();
            //RegisterComponentArray<Team>();
            //RegisterComponentArray<Controllable>();
        }

        public delegate void StartDelegate();
        public delegate void UpdateDelegate();
        //public delegate void UpdateIteratorDelegate(GameEntitySystem system, int id, int dt);
        //public delegate void UpdateIteratorDelegate<C1, C2>(GameEntitySystem system, C1 c1, C2 c2, int dt);

        public event StartDelegate EvtStart;
        public event UpdateDelegate EvtUpdate;
        //public event UpdateIteratorDelegate EvtUpdateIterator;

        public void Start()
        {
            if (EvtStart != null)
                EvtStart();
        }

        public int DeltaTime;

        public void Update(int dt)
        {
            DeltaTime = dt;
            //OptimizeArrays();

            if (EvtUpdate != null)
                EvtUpdate();

            //if (EvtUpdateIterator != null)
            //{
            //    for (int i = 0; i < EntityCount; ++i)
            //    {
            //        MovementProcessor.OnUpdateIterator(this, i, dt);
            //        DamageProcessor.OnUpdateIterator(this, i, dt);
            //        //EvtUpdateIterator(this, i, dt);
            //    }
            //}
        }

    }
}