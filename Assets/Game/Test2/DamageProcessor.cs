﻿using CFES;
using System.Collections.Generic;

namespace Test2
{
    public class DamageProcessor : Processor<Transform, Team, Velocity>
    {
        static int Mask = Component<Transform>.Mask | Component<Team>.Mask | Component<Velocity>.Mask;

        public DamageProcessor(GameEntitySystem system, SpatialProcessor spatial)
            : base(system)
        {
            m_system = system;
            m_spatial = spatial;

            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;

            m_processFunc = ProcessEntity;
        }

        private void OnStart()
        {

        }

        List<int> destroy = new List<int>();

        private void OnUpdate()
        {
            UnityEngine.Profiling.Profiler.BeginSample("DamageProcessor.OnUpdate");

            int count = 0;
            Transform[] transform = GetComponentArray1();
            Team[] team = GetComponentArray2(); 
            Velocity[] velocity = GetComponentArray3();

            destroy.Clear();

            var entities = GetEntityList();

            for(int i = 0; i < entities.Count; ++i)
            //for(; Iterate(ref i); i.Next())
            {
                var e1 = entities[i];
                //Iterator nearestID;
                long nearestDistance = int.MaxValue;

                var t1 = transform[e1.i1];

                var entities2 = m_spatial.GetEntities(t1.x, t1.y);

                for(int j = 0; j < entities2.Count; ++j)
                //for(int j = i+1; j < entities.Count; ++j)
                //for(j = i; Iterate(ref j); j.Next())
                {
                    var e2 = m_system.GetEntity(entities2[j]);

                    if (e2.ID <= e1.entityID)
                        continue;

                    if(!e2.CompareMask(Mask))
                        continue;

                    var team2 = m_system.GetComponent<Team>(e2);

                    if (team[e1.i2].teamID == team2.teamID)
                        continue;
                    
                    var t2 = m_system.GetComponent<Transform>(e2);

                    //var e2 = entities[j];

                    //count++;
                    //if (team[e1.i2].teamID == team[e2.i2].teamID)
                    //    continue;
                    //
                    //var t2 = transform[e2.i1];
            
                    //var dx = t2.x - t1.x;
                    //var dy = t2.y - t1.y;
                    //long d = dx * dx + dy * dy;
            
                    var dot = FixMath.Dot(t1.x, t1.y, t2.x, t2.y);
            
                    if (dot < nearestDistance)
                    {
                        nearestDistance = dot;
                        //nearestID = j;
                    }
            
                    if (dot < FixMath.FromInt(1))
                    {
                        if(!destroy.Contains(e1.entityID))
                            destroy.Add(e1.entityID);
                        if(!destroy.Contains(e2.ID))
                            destroy.Add(e2.ID);
                    }
                }
            
                //if (nearestID != -1)
                //{
                //    int dx = system.Transform[nearestID].x - system.Transform[i].x;
                //    int dy = system.Transform[nearestID].y - system.Transform[i].y;
                //    int maxS = FixMath.FromInt(1);
                //    if (dx > maxS)
                //        dx = maxS;
                //    if (dx < -maxS)
                //        dx = -maxS;
                //    if (dy > maxS)
                //        dy = maxS;
                //    if (dy < -maxS)
                //        dy = -maxS;
                //    system.Velocity[i].x = dx;
                //    system.Velocity[i].y = dy;
                //
                //    //UnityEngine.Debug.Log(FixMath.Atan2(dx, dy));
                //}
                
            }

            UnityEngine.Profiling.Profiler.BeginSample("Destroy");

            for (int d = 0; d < destroy.Count; ++d)
                m_system.DestroyEntity(destroy[d]);

            UnityEngine.Profiling.Profiler.EndSample();

            //UnityEngine.Debug.Log(count);
            UnityEngine.Profiling.Profiler.EndSample();

        }

        private void ProcessEntity(int entityID, ref Transform transform, ref Team team, ref Velocity vel)
        {
        }

        private Processor<Transform, Team, Velocity>.Func m_processFunc;
        private Processor<Transform, Team, Velocity> m_processor;

        private GameEntitySystem m_system;
        private SpatialProcessor m_spatial;
    }
}