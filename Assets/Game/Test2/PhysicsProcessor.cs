﻿using CFES;

namespace Test2
{
    public class PhysicsProcessor : Processor<Transform, Velocity>
    {
        public PhysicsProcessor(GameEntitySystem system) : base(system)
        {
            m_system = system;
            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;
            m_processFunc = ProcessMovement;
        }

        private void OnStart()
        {
        }

        private void OnUpdate()
        {
            Process(m_processFunc);
        }

        private void ProcessMovement(int entityID, ref Transform transform, ref Velocity velocity)
        {
            transform.x += FixMath.Multiply(velocity.x, m_system.DeltaTime);
            transform.y += FixMath.Multiply(velocity.y, m_system.DeltaTime);
        }

        private Func m_processFunc;
        private GameEntitySystem m_system;
    }
}