﻿using CFES;

namespace Test2
{
    public class MovementProcessor : Processor<Transform, Velocity, MoveTarget>
    {
        public MovementProcessor(GameEntitySystem system) : base(system)
        {
            m_system = system;
            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;
            m_processFunc = ProcessMovement;
        }

        private void OnStart()
        {
        }

        private void OnUpdate()
        {
            Process(m_processFunc);
        }

        private void ProcessMovement(int entityID, ref Transform transform, ref Velocity velocity, ref MoveTarget target)
        {
            int toTargetX = target.x - transform.x;
            int toTargetY = target.y - transform.y;

            int dist = FixMath.Length(toTargetX, toTargetY);//(int)FixMath.Lsqrt((long)toTargetX * toTargetX + (long)toTargetY * toTargetY);

            int tdist = FixMath.FromInt(1);

            if (dist > tdist)
            {
                int spd = FixMath.FromInt(10);
                velocity.x = toTargetX * spd / dist;
                velocity.y = toTargetY * spd / dist;

                //FixMath.ClampMagnitude(ref velocity.x, ref velocity.y, spd);
            }
            else
            {
                velocity.x /= 2;
                velocity.y /= 2;
            }
        }

        private Func m_processFunc;
        private GameEntitySystem m_system;
    }
}