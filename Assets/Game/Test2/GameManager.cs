﻿using System;
using CFES;

namespace Test2
{
    public class GameManager
    {
        public GameManager(GameEntitySystem system)
        {
            m_system = system;

            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;

            new PhysicsProcessor(system);
            new MovementProcessor(system);
            new DamageProcessor(system, new SpatialProcessor(system));
        }

        private void OnStart()
        {
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Velocity.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Graphic.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask | Velocity.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask | Graphic.Mask);

            UnityEngine.Random.InitState(0);
            for (int i = 0; i < 1000; ++i)
            {
                //if(UnityEngine.Random.value > 0.5f)
                //    system.CreateEntity(CreateObject);
                //else
                //m_system.CreateEntity(CreateObject);

                Prefabs.CreateUnit(m_system,
                    UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)),
                    UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)),
                    UnityEngine.Random.Range(0, 2));

            }

            var team1 = CreateTeam();
            var team2 = CreateTeam();
        }

        private void OnUpdate()
        {
            //for(int i = 0; i < 100; ++i)
            //    CreateTestObject(system, UnityEngine.Random.Range(0, system.EntityCount));
        }

        private void CreateTestObject(Entity ptr)
        {
            if (UnityEngine.Random.value > 0.5f)
                m_system.SetComponent(ptr, new Velocity(
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1)),
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1))));
            
            if (UnityEngine.Random.value > 0.5f)
                m_system.SetComponent(ptr, new Transform(
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)),
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)), 0));
            
            var team = new Team(UnityEngine.Random.Range(0, 2));
            
            if (UnityEngine.Random.value > 0.5f)
                m_system.SetComponent(ptr, team);

            if (UnityEngine.Random.value > 0.5f)
            {
                m_system.SetComponent(ptr, new Graphic(team.teamID));
                m_system.SetComponent<Controllable>(ptr, new Controllable());
            }

//            if (UnityEngine.Random.value > 0.5f)
        }

        private Entity CreateTeam()
        {
            var teamData = new TeamData();
            teamData.maxEnergy = 1000;
            teamData.maxMetal = 1000;

            var entity = m_system.CreateEntity();
            m_system.SetComponent<TeamData>(entity, teamData);
            return entity;
        }

        private GameEntitySystem m_system;
    }
}