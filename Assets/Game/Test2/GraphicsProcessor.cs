﻿using UnityEngine;
using CFES;
using System.Collections.Generic;

namespace Test2
{
    public class GraphicsProcessor : Processor<Transform, Graphic>
    {
        static int Mask = Component<Transform>.Mask | Component<Graphic>.Mask;

        public World world;

        public GraphicsProcessor(GameEntitySystem system) : base(system)
        {
            m_system = system;

            //system.EvtEntityCreated += OnEntityCreated;
            system.EvtEntityDestroyed += OnEntityDestroyed;
            system.EvtUpdate += OnUpdate;
            //system.EvtUpdateIterator += OnUpdateIterator;

            //drawCommands = new DrawCommand[system.EntityCount];

            m_entityObjects = new Dictionary<int, UnityEngine.Transform>();

            m_processFunc = ProcessEntity;

            //meshes = new Mesh[world.graphics.Length];
            //materials = new Material[world.graphics.Length];
            //for (int i = 0; i < world.graphics.Length; ++i)
            //{
            //    meshes[i] = world.graphics[i].GetComponent<MeshFilter>().sharedMesh;
            //    materials[i] = world.graphics[i].GetComponent<MeshRenderer>().sharedMaterial;
            //}
        }


        private void OnUpdate()
        {
            Process(m_processFunc);

            //var transforms = system.GetComponentStorage<Transform>();
            //var graphics = system.GetComponentStorage<Graphic>();
            //
            //for (int i = 0; i < m_objectCount; ++i)
            //{
            //    var obj = m_entityObjects[i];
            //    if (obj.entity.CompareMask(Mask))
            //    {
            //        var transform = transforms.GetComponent(obj.entity.ID);
            //        var graphic = graphics.GetComponent(obj.entity.ID);
            //        ProcessEntity(system,
            //            ref obj,
            //            ref transform,
            //            ref graphic);
            //    }
            //    
            //}

            //foreach(int i in system.GetEntities(Graphic.Mask))
            //{
            //    var transform = system.Transform[i];
            //    Graphics.DrawMesh(drawCommands[i].mesh, new Vector3(transform.x, 0, transform.y), Quaternion.identity, drawCommands[i].material, 0);
            //}
        }

        private void ProcessEntity(int entityID, ref Transform transform, ref Graphic graphic)
        {
            //var mesh = meshes[system.Graphic[id].graphicID];
            //var material = materials[system.Graphic[id].graphicID];

            //drawCommands[id].mesh = mesh;
            //drawCommands[id].material = material;

            //Graphics.DrawMesh(mesh, new Vector3((float)transform.position.X, 0, (float)transform.position.Y), Quaternion.identity, material, 0);

            UnityEngine.Transform t;
            if (m_entityObjects.TryGetValue(entityID, out t))
            {
                t.position = new Vector3(FixMath.ToFloat(transform.x), 0, FixMath.ToFloat(transform.y));
            }
            else
            {
                CreateGraphic(entityID, ref transform, ref graphic);
            }
        }

        //void OnEntityCreated(Entity ptr)
        //{
        //    if (ptr.CompareMask(Mask))
        //    {
        //        int i = m_objectCount++;
        //        var graphic = m_system.GetComponent<Graphic>(ptr);
        //        var transform = GameObject.Instantiate(world.graphics[graphic.graphicID]).transform;
        //        transform.name = ptr.ID.ToString();
        //        m_entityObjects.Add(ptr.ID, transform);
        //    }
        //}

        private void CreateGraphic(int entityID, ref Transform transform, ref Graphic graphic)
        {
            var t = GameObject.Instantiate(world.graphics[graphic.graphicID]).transform;
            t.name = entityID.ToString();
            m_entityObjects.Add(entityID, t);
        }

        void OnEntityDestroyed(Entity ptr)
        {
            UnityEngine.Transform t;
            if (m_entityObjects.TryGetValue(ptr.ID, out t))
            {
                GameObject.Destroy(t.gameObject);
                m_entityObjects.Remove(ptr.ID);
            }
        }

        //private Mesh[] meshes;
        //private Material[] materials;

        //private DrawCommand[] drawCommands;
        //private struct DrawCommand
        //{
        //    public Mesh mesh;
        //    public Material material;
        //}

        private Dictionary<int, UnityEngine.Transform> m_entityObjects;
        private int m_objectCount;
        private GameEntitySystem m_system;
        private Func m_processFunc;
        //private UnityEngine.Transform[] m_entityObjects;
        //private CullingGroup m_cullingGroup;
    }
}