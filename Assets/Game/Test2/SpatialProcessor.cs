﻿using CFES;
using Test2;
using System.Collections.Generic;

public class SpatialProcessor : Processor<Transform, SpatialQueryable>
{
    public List<int> GetEntities(int x, int y)
    {
        int cellX, cellY;
        GetCell(x, y, out cellX, out cellY);
        return m_spatialMap[cellX][cellY];
    }

    public SpatialProcessor(GameEntitySystem system) : base(system)
    {
        CellsX = MapWidth / CellWidth;
        CellsY = MapHeight / CelHeight;
        OriginX = MapWidth / 2;
        OriginY = MapHeight / 2;

        m_system = system;
        m_processFunc = Process;

        m_spatialMap = new List<int>[CellsX][];
        for (int i = 0; i < CellsX; ++i)
        {
            m_spatialMap[i] = new List<int>[CellsY];
        }

        for (int i = 0; i < CellsX; ++i)
        for (int j = 0; j < CellsY; ++j)
            m_spatialMap[i][j] = new List<int>();

        m_system.EvtUpdate += Update;
    }

    private void Update()
    {
        //debugStr = "";
        for (int i = 0; i < CellsX; ++i)
        for (int j = 0; j < CellsY; ++j)
        {
            //debugStr += i + "," + j + " " + m_spatialMap[i][j].Count;
            m_spatialMap[i][j].Clear();
        }
        //UnityEngine.Debug.Log(debugStr);

        Process(m_processFunc);
    }

    private void Process(int entityID, ref Transform transform, ref SpatialQueryable spatial)
    {
        int cellX, cellY;
        GetCell(transform.x, transform.y, out cellX, out cellY);
        m_spatialMap[cellX][cellY].Add(entityID);
    }

    private void GetCell(int x, int y, out int cellX, out int cellY)
    {
        cellX = (x + OriginX) / CellWidth;
        cellY = (y + OriginY) / CelHeight;

        if (cellX < 0 || cellX > CellsX ||
            cellY < 0 || cellY > CellsY)
            throw new System.Exception("Out of bounds: " + cellX + " " + cellY);
    }

    private string debugStr;

    private readonly int MapWidth = FixMath.FromInt(128);
    private readonly int MapHeight = FixMath.FromInt(128);
    private readonly int CellWidth = FixMath.FromInt(8);
    private readonly int CelHeight = FixMath.FromInt(8);
    private readonly int CellsX;
    private readonly int CellsY;

    private readonly int OriginX;
    private readonly int OriginY;

    private GameEntitySystem m_system;
    private Func m_processFunc;

    private List<int>[][] m_spatialMap;
}
