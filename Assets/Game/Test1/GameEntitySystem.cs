﻿using UnityEngine;
using System.Collections;

namespace Test1
{
    public class GameEntitySystem : CFES.SortedEntitySystem<GameEntitySystem>
    {
        public const int MaxEntities = 1000;

        #region ENTITY COMPONENTS
        public readonly Transform[] Transform = new Transform[MaxEntities];
        public readonly Velocity[] Velocity = new Velocity[MaxEntities];
        public readonly Graphic[] Graphic = new Graphic[MaxEntities];
        public readonly Team[] Team = new Team[MaxEntities];
        #endregion

        #region SINGLETON COMPONENTS

        #endregion

        public GameEntitySystem()
            : base(MaxEntities)
        {
            RegisterComponentArray(Transform);
            RegisterComponentArray(Velocity);
            RegisterComponentArray(Graphic);
            RegisterComponentArray(Team);
        }

        public delegate void StartDelegate(GameEntitySystem system);
        public delegate void UpdateDelegate(GameEntitySystem system, int dt);
        //public delegate void UpdateIteratorDelegate(GameEntitySystem system, int id, int dt);
        //public delegate void UpdateIteratorDelegate<C1, C2>(GameEntitySystem system, C1 c1, C2 c2, int dt);

        public event StartDelegate EvtStart;
        public event UpdateDelegate EvtUpdate;
        //public event UpdateIteratorDelegate EvtUpdateIterator;

        public void Start()
        {
            if (EvtStart != null)
                EvtStart(this);
        }

        public void Update(int dt)
        {
            //OptimizeArrays();

            if (EvtUpdate != null)
                EvtUpdate(this, dt);

            //if (EvtUpdateIterator != null)
            //{
            //    for (int i = 0; i < EntityCount; ++i)
            //    {
            //        MovementProcessor.OnUpdateIterator(this, i, dt);
            //        DamageProcessor.OnUpdateIterator(this, i, dt);
            //        //EvtUpdateIterator(this, i, dt);
            //    }
            //}
        }

    }
}