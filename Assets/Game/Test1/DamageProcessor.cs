﻿using CFES;

namespace Test1
{
    public static class DamageProcessor
    {
        static int Mask = Component<Transform>.Mask | Component<Team>.Mask | Component<Velocity>.Mask;

        public static void Register(GameEntitySystem system)
        {
            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;
        }

        private static void OnStart(GameEntitySystem system)
        {

        }

        public static void OnUpdateIterator(GameEntitySystem system, int id, int dt)
        {
            if (system.CompareMask(Mask, id))
            {
                ProcessEntity(system, ref system.Transform[id], ref system.Graphic[id], id);
            }
        }

        private static void ProcessEntity(GameEntitySystem system, ref Transform transform, ref Graphic graphic, int id)
        {
        }

        private static void OnUpdate(GameEntitySystem system, int dt)
        {
            int count = 0;
            for (int i = 0; i < system.EntityCount; ++i)
            {
                if (!system.CompareMask(Mask, i))
                    continue;

                //ProcessEntity(system, ref system.Transform[i], ref system.Graphic[i], i);

                int nearestID = -1;
                long nearestDistance = int.MaxValue;

                for (int j = i + 1; j < system.EntityCount; ++j)
                {
                    count++;

                    if (!system.CompareMask(Mask, j))
                        continue;

                    if (system.Team[i].teamID == system.Team[j].teamID)
                        continue;

                    var t1 = system.Transform[i];
                    var t2 = system.Transform[j];

                    //var dx = t2.x - t1.x;
                    //var dy = t2.y - t1.y;
                    //long d = dx * dx + dy * dy;

                    var dot = FixMath.Dot(t1.x, t1.y, t2.x, t2.y);

                    if (dot < nearestDistance)
                    {
                        nearestDistance = dot;
                        nearestID = j;
                    }

                    if (dot < FixMath.FromInt(1))
                    {
                        system.DestroyEntity(i);
                        system.DestroyEntity(j);
                    }
                }

                //if (nearestID != -1)
                //{
                //    int dx = system.Transform[nearestID].x - system.Transform[i].x;
                //    int dy = system.Transform[nearestID].y - system.Transform[i].y;
                //    int maxS = FixMath.FromInt(1);
                //    if (dx > maxS)
                //        dx = maxS;
                //    if (dx < -maxS)
                //        dx = -maxS;
                //    if (dy > maxS)
                //        dy = maxS;
                //    if (dy < -maxS)
                //        dy = -maxS;
                //    system.Velocity[i].x = dx;
                //    system.Velocity[i].y = dy;
                //
                //    //UnityEngine.Debug.Log(FixMath.Atan2(dx, dy));
                //}
            }

            //UnityEngine.Debug.Log(count);
        }
    }
}