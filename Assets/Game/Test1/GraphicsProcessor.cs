﻿using UnityEngine;
using CFES;

namespace Test1
{
    public class GraphicsProcessor
    {
        static int Mask = Component<Graphic>.Mask;

        public World world;

        public void Register(GameEntitySystem system)
        {
            system.EvtEntityCreated += OnEntityCreated;
            //system.EvtEntityDestroyed += OnEntityDestroyed;
            system.EvtUpdate += OnUpdate;
            //system.EvtUpdateIterator += OnUpdateIterator;

            //drawCommands = new DrawCommand[system.EntityCount];

            m_entityObjects = new EntityObject[system.EntityCount];

            //meshes = new Mesh[world.graphics.Length];
            //materials = new Material[world.graphics.Length];
            //for (int i = 0; i < world.graphics.Length; ++i)
            //{
            //    meshes[i] = world.graphics[i].GetComponent<MeshFilter>().sharedMesh;
            //    materials[i] = world.graphics[i].GetComponent<MeshRenderer>().sharedMaterial;
            //}
        }


        private void OnUpdate(GameEntitySystem system, int dt)
        {
            for (int i = 0; i < m_entityObjects.Length; ++i)
            {
                if (m_entityObjects[i].transform != null)
                {
                    var obj = m_entityObjects[i];
                    ProcessEntity(system,
                        ref obj,
                        ref system.Transform[system.ResolvePtr(m_entityObjects[i].entityPtr)],
                        ref system.Graphic[system.ResolvePtr(m_entityObjects[i].entityPtr)]);

                }
            }
            //foreach(int i in system.GetEntities(Graphic.Mask))
            //{
            //    var transform = system.Transform[i];
            //    Graphics.DrawMesh(drawCommands[i].mesh, new Vector3(transform.x, 0, transform.y), Quaternion.identity, drawCommands[i].material, 0);
            //}
        }

        private void ProcessEntity(GameEntitySystem system, ref EntityObject obj, ref Transform transform, ref Graphic graphic)
        {
            //var mesh = meshes[system.Graphic[id].graphicID];
            //var material = materials[system.Graphic[id].graphicID];

            //drawCommands[id].mesh = mesh;
            //drawCommands[id].material = material;

            //Graphics.DrawMesh(mesh, new Vector3((float)transform.position.X, 0, (float)transform.position.Y), Quaternion.identity, material, 0);

            obj.transform.position = new Vector3(FixMath.ToFloat(transform.x), 0, FixMath.ToFloat(transform.y));
        }

        void OnEntityCreated(GameEntitySystem system, EntityPtr ptr)
        {
            if (system.CompareMask(Mask, ptr))
            {
                int i = m_objectCount++;
                var obj = new EntityObject();
                obj.entityPtr = ptr;
                obj.transform = GameObject.Instantiate(world.graphics[system.Graphic[system.ResolvePtr(ptr)].graphicID]).transform;
                obj.transform.name = obj.entityPtr.ToString();
                m_entityObjects[i] = obj;
            }
        }

        void OnEntityDestroyed(GameEntitySystem system, EntityPtr ptr)
        {
            //if (m_entityObjects[id] != null)
            //{
            //    GameObject.Destroy(m_entityObjects[id].gameObject);
            //}
        }

        //private Mesh[] meshes;
        //private Material[] materials;

        //private DrawCommand[] drawCommands;
        //private struct DrawCommand
        //{
        //    public Mesh mesh;
        //    public Material material;
        //}

        private struct EntityObject
        {
            public EntityPtr entityPtr;
            public UnityEngine.Transform transform;
        }

        private EntityObject[] m_entityObjects;
        private int m_objectCount;
        //private UnityEngine.Transform[] m_entityObjects;
        //private CullingGroup m_cullingGroup;
    }
}