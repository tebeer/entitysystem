﻿using CFES;

namespace Test1
{
    public static class MovementProcessor
    {
        static int Mask = Component<Transform>.Mask | Component<Velocity>.Mask;

        public static void Register(GameEntitySystem system)
        {
            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;
            //system.EvtUpdateIterator += OnUpdateIterator;
        }

        private static void OnStart(GameEntitySystem system)
        {
        }

        private static void OnUpdate(GameEntitySystem system, int dt)
        {
            //for (int i = 0; i < system.EntityCount; ++i)
            //{
            //    if (system.CompareMask(Mask, i))
            //    {
            //        ProcessMovement(ref system.Transform[i], ref system.Velocity[i], dt);
            //    }
            //}
            system.GetSignature<Transform, Velocity>().ForEach(m_processFunc);
            //system.ForEach(ProcessMovement);
        }

        //private static CFES.Signature<Transform, Velocity>.ProcessFunc ProcessMovement = (ref Transform t, ref Velocity v) =>
        //{
        //    t.x += FixMath.Multiply(v.x, 12);
        //    t.y += FixMath.Multiply(v.y, 12);
        //};

        private static void ProcessMovement2(ref Transform transform, ref Velocity velocity)
        {
            transform.x += FixMath.Multiply(velocity.x, 12);
            transform.y += FixMath.Multiply(velocity.y, 12);
        }

        private static void ProcessMovement(ref Transform transform, ref Velocity velocity, int dt)
        {
            transform.x += FixMath.Multiply(velocity.x, dt);
            transform.y += FixMath.Multiply(velocity.y, dt);
        }

        private static Signature<Transform, Velocity>.ProcessFunc m_processFunc = ProcessMovement2;
    }
}