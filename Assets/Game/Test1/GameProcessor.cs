﻿using System;
using CFES;

namespace Test1
{
    public static class GameProcessor
    {
        static int testMask = Component<Transform>.Mask | Component<Velocity>.Mask | Component<Graphic>.Mask;

        public static void Register(GameEntitySystem system)
        {
            system.EvtStart += OnStart;
            system.EvtUpdate += OnUpdate;

            MovementProcessor.Register(system);
            DamageProcessor.Register(system);
        }

        private static void OnStart(GameEntitySystem system)
        {
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Velocity.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Graphic.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask | Velocity.Mask);
            //system.CreateEntity((s, i) => s.Mask[i] = Transform.Mask | Graphic.Mask);

            UnityEngine.Random.InitState(0);
            for (int i = 0; i < system.EntityCount; ++i)
            {
                //if(UnityEngine.Random.value > 0.5f)
                //    system.CreateEntity(CreateObject);
                //else
                system.CreateEntity(CreateObject, false);

            }
        }

        private static void OnUpdate(GameEntitySystem system, int dt)
        {
            //for(int i = 0; i < 100; ++i)
            //    CreateTestObject(system, UnityEngine.Random.Range(0, system.EntityCount));
        }

        private static void CreateTestObject(GameEntitySystem system, EntityPtr ptr)
        {
            if (UnityEngine.Random.value > 0.5f)
                system.SetComponent(ptr, new Velocity(
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1)),
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1))));

            if (UnityEngine.Random.value > 0.5f)
                system.SetComponent(ptr, new Transform(
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)),
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)), 0));

            var team = new Team(UnityEngine.Random.Range(0, 2));

            if (UnityEngine.Random.value > 0.5f)
                system.SetComponent(ptr, team);

            if (UnityEngine.Random.value > 0.5f)
                system.SetComponent(ptr, new Graphic(team.teamID));

        }

        private static void CreateObject(GameEntitySystem system, EntityPtr ptr)
        {
            system.SetComponent(ptr, new Velocity(
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1)),
                UnityEngine.Random.Range(FixMath.FromInt(-1), FixMath.FromInt(1))));
            system.SetComponent(ptr, new Transform(
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)),
                UnityEngine.Random.Range(FixMath.FromInt(-50), FixMath.FromInt(50)), 0));
            var team = new Team(UnityEngine.Random.Range(0, 2));
            system.SetComponent(ptr, team);
            system.SetComponent(ptr, new Graphic(team.teamID));
        }
    }
}