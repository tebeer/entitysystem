﻿using CFES;
using Test2;

public static class Prefabs
{
    public static int CreateUnit(GameEntitySystem system, int x, int y, int teamID)
    {
        var e = system.CreateEntity();
        system.SetComponent(e, new Velocity());
        system.SetComponent(e, new Transform(x, y, 0));
        var team = new Team(teamID);
        system.SetComponent(e, team);
        system.SetComponent(e, new Graphic(team.teamID));
        system.SetComponent(e, new Controllable());
        system.SetComponent(e, new MoveTarget());
        system.SetComponent(e, new SpatialQueryable());
        return e.ID;
    }
}
