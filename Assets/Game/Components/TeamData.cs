﻿public struct TeamData : CFES.IComponent
{
    public int energy;
    public int metal;
    public int maxEnergy;
    public int maxMetal;
}
