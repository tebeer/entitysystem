﻿
public struct Velocity : CFES.IComponent
{    
    public int x;
    public int y;

    public Velocity(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
