﻿
public struct Transform : CFES.IComponent
{
    public int x;
    public int y;
    public int angle;

    public Transform(int x, int y, int angle)
    {
        this.x = x;
        this.y = y;
        this.angle = angle;
    }
}
