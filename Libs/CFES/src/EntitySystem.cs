﻿using System;
using System.Collections;
using System.Collections.Generic;

// System<Draw,Move> wants to filter all entities who have at least Draw and Move component,
// so the system itself should have a mask (bitset) (same size as entity's bit mask) as well.
// 
// The system also has a list of entity (or entity id) which records the filtered entites.
// (in your case, that's the vector<Entity> draw)(so this list should belong to a system)
// 
// When an entity add a component, the entity updates its component bit mask, and then try
// to match the masks with all systems; when a match occurs, that system register the entity
// onto its list, then the filtering is done.
// 
// On the next system's update, the system check the list, get the first entity id, then get
// the components from c1, c2 by this id, update those components, then move to the next entity
// id and repeat.

namespace CFES
{
    public class EntitySystem<SystemType>
        where SystemType : EntitySystem<SystemType>
    {
        public event System.Action<SystemType, int> EvtEntityCreated;
        public event System.Action<SystemType, int> EvtEntityDestroyed;

        public readonly int EntityCount;

        public readonly int[] Mask;
        public readonly int[] EntityPtr;
        public readonly int[] EntityPtr2;

        public IEnumerable<int> GetEntities(int mask)
        {
            for (int i = 0; i < EntityCount; ++i)
            {
                if (CompareMask(mask, i))
                    yield return i;
            }
        }

        public void ForEach(int mask, Action<SystemType, int> func)
        {
            for (int i = 0; i < EntityCount; ++i)
            {
                if (CompareMask(mask, i))
                    func((SystemType)this, i);
            }
        }

        public bool CompareMask(int mask, int entityID)
        {
            return (Mask[entityID] & mask) == mask;
        }

        public int GetPointer(int index)
        {
            for (int i = 0; i < EntityCount; ++i)
                if (EntityPtr[i] == index)
                    return i;
            throw new Exception("Pointer not found: " + index);
        }


        public int CreateEntity(Action<SystemType, int> initFunc)
        {
            for (int i = 0; i < EntityCount; ++i)
            {
                if (Mask[i] == 0)
                {
                    if (initFunc != null)
                        initFunc((SystemType)this, i);

                    if (EvtEntityCreated != null)
                        EvtEntityCreated((SystemType)this, i);

                    return i;
                }
            }
            return -1;
        }

        public void DestroyEntity(int i)
        {
            // TODO: Find entityID
            Mask[i] = 0;
            if (EvtEntityDestroyed != null)
                EvtEntityDestroyed((SystemType)this, i);
        }

        public EntitySystem(int entityCount)
        {
            EntityCount = entityCount;

            Mask = new int[entityCount];
            EntityPtr = new int[entityCount];
            EntityPtr2 = new int[entityCount];
            for (int i = 0; i < entityCount; ++i)
                EntityPtr[i] = i;

            m_sortMask = new int[entityCount];
            m_arrays = new List<IGenericArray>();
            //m_arrays.Add(new GenericArray<int>(EntityPtr));
        }

        public void OptimizeArrays()
        {
            // Sort arrays for fast iteration by cache coherency
            for (int i = 0; i < EntityCount; ++i)
                m_sortMask[i] = i;

            //Quicksort(0, Mask.Length-1);
            Array.Sort(Mask, m_sortMask);

            Array.Copy(EntityPtr, EntityPtr2, EntityCount);

            // Update pointers
            for (int i = 0; i < EntityCount; ++i)
                EntityPtr[m_sortMask[i]] = EntityPtr2[i];

            // Back trace the sort
            // Sort only needed elements
            int arrayCount = m_arrays.Count;
            int index = 0;
            int direction = 1;
            int countSorted = 0;
            while(true)
            {
                int fromI = m_sortMask[index];
                if (m_sortMask[index] == index)
                {
                    if (index == EntityCount - 1)
                        direction = -1;
                    if (index == 0)
                        direction = 1;
                    index += direction;

                    countSorted++;
                    if (countSorted == EntityCount)
                        break;

                    continue;
                }

                countSorted = 0;

                // Don't swap the first element 2nd time
                if (m_sortMask[fromI] != fromI)
                {
                    for (int k = 0; k < arrayCount; ++k)
                        m_arrays[k].Swap(index, fromI);
                }

                m_sortMask[index] = index;

                index = fromI;
            }
        }

        private void Quicksort(int left, int right)
        {
            int i = left, j = right;
            int pivot = Mask[(left + right) / 2];
            
            while (i <= j)
            {
                while (Mask[i] < pivot)
                    i++;
            
                while (Mask[j] > pivot)
                    j--;
            
                if (i <= j)
                {
                    var tmp = Mask[i];
                    Mask[i] = Mask[j];
                    Mask[j] = tmp;

                    tmp = m_sortMask[i];
                    m_sortMask[i] = m_sortMask[j];
                    m_sortMask[j] = tmp;

                    i++;
                    j--;
                }
            }
            
            if (left < j)
                Quicksort(left, j);
            
            if (i < right)
                Quicksort(i, right);
        }
 
        public void RegisterComponentArray<T>(T[] array)
        {
            m_arrays.Add(new GenericArray<T>(array));
        }

        private List<IGenericArray> m_arrays;
        private int[] m_sortMask;

        private interface IGenericArray
        {
            void GetTemp(int i);
            void SwapTemp(int i);

            void Swap(int i1, int i2); 
        }

        private class GenericArray<T> : IGenericArray
        {
            public T[] array;
            private T tempValue;

            public GenericArray(T[] array)
            {
                this.array = array;
            }

            public void GetTemp(int i)
            {
                tempValue = array[i];
            }

            public void SwapTemp(int i)
            {
                var t = tempValue;
                tempValue = array[i];
                array[i] = t;
            }

            public void Swap(int i1, int i2)
            {
                var tmp = array[i1];
                array[i1] = array[i2];
                array[i2] = tmp;
            }
        }

        public void Destroy()
        {
        }
    }
}

// Array of structs is slightly faster to iterate than struct of arrays, if it contains only the required data
//public Entity[] Entities;
//public struct Entity
//{
//    public Transform transform;
//    public Velocity velocity;
//}