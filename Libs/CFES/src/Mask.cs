﻿namespace CFES
{
    public interface IComponent
    {
    }
    
    public static class Component<T> where T : struct, IComponent
    {
        static Component()
        {
            ID = MaskCounter.Next;

            // Components that are defined first are the most significant
            // They will get the biggest contiguous blocks of memory
            //Mask = 1 << (30 - ID);
            Mask = 1 << ID;
        }

        public static readonly int ID;
        public static readonly int Mask;
    }

    public static class MaskCounter
    {
        public static int Next
        {
            get
            {
                return nextID++;
            }
        }

        public static int Current
        {
            get
            {
                return nextID;
            }
        }

        private static int nextID = 0;
    }
}