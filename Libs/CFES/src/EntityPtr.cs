﻿
namespace CFES
{
    public struct EntityPtr
    {
        internal EntityPtr(int ptr)
        {
            this.ptr = ptr;
        }
        internal int ptr;
    }
}
