﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFES
{
    public class Processor<T1>
        where T1 : struct, IComponent
    {
        public Processor(IEntitySystem system)
        {
            m_t1s = system.GetComponentStorage<T1>();
        }

        public T1[] GetComponentArray()
        {
            return m_t1s.Components;
        }

        public delegate void Func(int id, ref T1 c1);

        public void Process(Func func)
        {
            for (Iterator i = new Iterator(); Iterate(ref i); i.Next())
                func(i.entityID, ref m_t1s.Components[i.i]);
        }

        public bool Iterate(ref Iterator iterator)
        {
            if (iterator.i < m_t1s.Count)
            {
                iterator.entityID = m_t1s.EntityIDs[iterator.i];
                return true;
            }
            return false;
        }

        public struct Iterator
        {
            public int entityID;
            public int i;
            public void Next()
            {
                i++;
            }
        }

        private ComponentStorage<T1> m_t1s;
    }

    public class Processor<T1, T2>
        where T1 : struct, IComponent
        where T2 : struct, IComponent
    {
        public Processor(IEntitySystem system)
        {
            m_t1s = system.GetComponentStorage<T1>();
            m_t2s = system.GetComponentStorage<T2>();
        }
        protected T1[] GetComponentArray1()
        {
            return m_t1s.Components;
        }

        protected T2[] GetComponentArray2()
        {
            return m_t2s.Components;
        }

        public List<Iterator> GetEntityList()
        {
            // Don't update if nothing changed
            if (m_t1Version == m_t1s.Version &&
                m_t2Version == m_t2s.Version)
                return m_entityList;

            m_t1Version = m_t1s.Version;
            m_t2Version = m_t2s.Version;

            m_entityList.Clear();
            Iterator i = new Iterator();
            for (; Iterate(ref i); i.Next())
                m_entityList.Add(i);

            return m_entityList;
        }

        public bool Iterate(ref Iterator i)
        {
            while (i.i1 < m_t1s.Count && i.i2 < m_t2s.Count)
            {
                if (m_t1s.EntityIDs[i.i1] < m_t2s.EntityIDs[i.i2])
                {
                    i.i1++;
                    continue;
                }
                if (m_t2s.EntityIDs[i.i2] < m_t1s.EntityIDs[i.i1])
                {
                    i.i2++;
                    continue;
                }

                i.entityID = m_t1s.EntityIDs[i.i1];
                return true;
            }
            return false;
        }

        public delegate void Func(int id, ref T1 c1, ref T2 c2);

        public void Process(Func func)
        {
            var entities = GetEntityList();

            for (int i = 0; i < entities.Count; ++i)
            {
                func(entities[i].entityID,
                    ref m_t1s.Components[entities[i].i1],
                    ref m_t2s.Components[entities[i].i2]);
            }
        }

        public struct Iterator
        {
            public int entityID;
            public int i1, i2;
            public void Next()
            {
                i1++;
                i2++;
            }
        }

        private readonly List<Iterator> m_entityList = new List<Iterator>();

        private ComponentStorage<T1> m_t1s;
        private ComponentStorage<T2> m_t2s;

        private int m_t1Version, m_t2Version;
    }

    public class Processor<T1, T2, T3>
        where T1 : struct, IComponent
        where T2 : struct, IComponent
        where T3 : struct, IComponent
    {
        public Processor(IEntitySystem system)
        {
            m_t1s = system.GetComponentStorage<T1>();
            m_t2s = system.GetComponentStorage<T2>();
            m_t3s = system.GetComponentStorage<T3>();
        }

        protected T1[] GetComponentArray1()
        {
            return m_t1s.Components;
        }

        protected T2[] GetComponentArray2()
        {
            return m_t2s.Components;
        }

        protected T3[] GetComponentArray3()
        {
            return m_t3s.Components;
        }

        public List<Iterator> GetEntityList()
        {
            // Don't update if nothing changed
            if (m_t1Version == m_t1s.Version &&
                m_t2Version == m_t2s.Version &&
                m_t3Version == m_t3s.Version)
                return m_entityList;

            m_t1Version = m_t1s.Version;
            m_t2Version = m_t2s.Version;
            m_t3Version = m_t3s.Version;

            m_entityList.Clear();
            Iterator i = new Iterator();
            for (; Iterate(ref i); i.Next())
                m_entityList.Add(i);

            return m_entityList;
        }

        public bool Iterate(ref Iterator i)
        {
            while (i.i1 < m_t1s.Count && i.i2 < m_t2s.Count && i.i3 < m_t3s.Count)
            {
                if (m_t1s.EntityIDs[i.i1] < m_t2s.EntityIDs[i.i2])
                {
                    i.i1++;
                    continue;
                }
                if (m_t2s.EntityIDs[i.i2] < m_t3s.EntityIDs[i.i3])
                {
                    i.i2++;
                    continue;
                }
                // t1 >= t2 && t2 >= t3
                if (m_t3s.EntityIDs[i.i3] < m_t1s.EntityIDs[i.i1] || m_t3s.EntityIDs[i.i3] < m_t2s.EntityIDs[i.i2])
                {
                    i.i3++;
                    continue;
                }

                i.entityID = m_t1s.EntityIDs[i.i1];
                return true;
            }
            return false;
        }

        public delegate void Func(int id, ref T1 c1, ref T2 c2, ref T3 c3);

        public void Process(Func func)
        {
            var entities = GetEntityList();

            for (int i = 0; i < entities.Count; ++i)
            {
                func(entities[i].entityID,
                    ref m_t1s.Components[entities[i].i1],
                    ref m_t2s.Components[entities[i].i2],
                    ref m_t3s.Components[entities[i].i3]);
            }
        }

        public struct Iterator
        {
            public int entityID;
            public int i1, i2, i3;
            public void Next()
            {
                i1++;
                i2++;
                i3++;
            }
        }

        private readonly List<Iterator> m_entityList = new List<Iterator>();

        private ComponentStorage<T1> m_t1s;
        private ComponentStorage<T2> m_t2s;
        private ComponentStorage<T3> m_t3s;

        private int m_t1Version, m_t2Version, m_t3Version;
    }

}
