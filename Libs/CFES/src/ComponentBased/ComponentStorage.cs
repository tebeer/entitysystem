﻿using System.Collections.Generic;

namespace CFES
{
    public abstract class ComponentStorage
    {
        public abstract int Count { get; }
        public abstract int Mask { get; }
        internal abstract void Resize(int i);
        internal abstract void RemoveComponent(int entityID);
    }

    public class ComponentStorage<T> : ComponentStorage where T : struct, IComponent
    {
        public override int Count { get { return m_currentSize; } }
        public override int Mask { get { return Component<T>.Mask; } }

        //public T[] GetComponentArray()
        //{
        //    return Components;
        //}

        internal T[] Components = new T[1];
        internal int[] EntityIDs = new int[1];

        internal int Version;

        private int m_currentSize;
        private bool m_allowResize;

        internal ComponentStorage(bool allowResize)
        {
            m_allowResize = allowResize;
        }

        internal override void Resize(int size)
        {
            if (!m_allowResize)
                throw new System.Exception("Component buffer full: " + typeof(T).ToString());
            if (size < Components.Length)
                throw new System.Exception("Cannot resize to smaller than current size");
        
            var newArray = new T[size];
            var newID = new int[size];
            System.Array.Copy(Components, 0, newArray, 0, Components.Length);
            System.Array.Copy(EntityIDs, 0, newID, 0, EntityIDs.Length);
            Components = newArray;
            EntityIDs = newID;
        
            //UnityEngine.Debug.Log("Resize " + typeof(T).ToString() + ": " + size + "\n" + str1 + "\n" + str2); 
        }

        internal void SetComponent(int entityID, T value)
        {
            //UnityEngine.Debug.Log("SetComponent " + entityID + " " + typeof(T).ToString());
            int i;
            if (FindIndex(entityID, out i))
            {
                Components[i] = value;
            }
            else
            {
                if (m_currentSize >= Components.Length)
                    Resize(Components.Length * 2);

                if (i != m_currentSize)
                {
                    i++;
                    //UnityEngine.Debug.Log("Insert " + index + " " + m_components.Length);
                    System.Array.Copy(Components, i, Components, i + 1, m_currentSize - i);
                    System.Array.Copy(EntityIDs, i, EntityIDs, i + 1, m_currentSize - i);
                }
                m_currentSize++;

                //i = m_currentSize++;
                Components[i] = value;
                EntityIDs[i] = entityID;
                //Sort();
                Version++;
            }
        }

        internal void RemoveComponentIndex(int i)
        {
            if (Components.Length > 1)
            {
                System.Array.Copy(Components, i + 1, Components, i, m_currentSize - i - 1);
                System.Array.Copy(EntityIDs, i + 1, EntityIDs, i, m_currentSize - i - 1);
            }
            m_currentSize--;
            //Swap(i, m_currentSize);
            //m_components[m_currentSize] = default(T);
            //m_entityIDs[m_currentSize] = 0;
            //Sort();
            Version++;
        }

        internal override void RemoveComponent(int entityID)
        {
            int i;
            if (FindIndex(entityID, out i))
            {
                RemoveComponentIndex(i);
                return;
            }

            throw new System.Exception("RemoveComponent: Entity " + entityID + " doesn't have component: " + typeof(T));
        }

        internal T GetComponent(int entityID)
        {
            int i;
            if (FindIndex(entityID, out i))
                return Components[i];
            throw new System.Exception("GetComponent: Entity " + entityID + " doesn't have component: " + typeof(T));
        }

        internal bool FindIndex(int entityID, out int i)
        {
            if (m_currentSize == 0)
            {
                i = 0;
                return false;
            }

            if (m_currentSize == 1)
            {
                i = 0;
                return EntityIDs[0] == entityID;
            }

            // Binary search index of entityID
            int l = 0;
            int r = m_currentSize - 1;
            i = 0;
            int iterations = 0;
            while (true)
            {
                if (iterations++ > 1000)
                {
                    //string str = "";
                    //for (int j = 0; j < EntityIDs.Length; ++j)
                    //    str += EntityIDs[j] + " ";
                    //UnityEngine.Debug.Log(str);
                    throw new System.Exception(string.Format("Too many iterations. l={0}, r={1}, i={2}, entityID={3}, EntityIDs[i]={4}, EntityIDs[i+1]={5}", l, r, i, entityID, EntityIDs[i], EntityIDs[i + 1]));
                }

                if(l >= r)
                {
                    i = l;
                    return EntityIDs[i] == entityID;
                }

                i = (l + r) / 2;

                // 783
                // 0 782, 391
                // 0 390, 195
                // 0 194, 97
                // 0 96, 48
                // 0 47, 23
                // 0 22, 11
                // 0 10, 5
                // 0 4, 2
                // 0 1, 0
                //UnityEngine.Debug.Log(l + " " + i + " " + r + " " + entityID);

                if (entityID > EntityIDs[i])
                    l = i + 1;
                else if (entityID < EntityIDs[i])
                    r = i - 1;
                else
                    return true;
            }

        }

        //public void Swap(int i1, int i2)
        //{
        //    var tmp = m_components[i1];
        //    m_components[i1] = m_components[i2];
        //    m_components[i2] = tmp;
        //
        //    var tmp2 = m_entityIDs[i1];
        //    m_entityIDs[i1] = m_entityIDs[i2];
        //    m_entityIDs[i2] = tmp2;
        //}
        //
        //private void Sort()
        //{
        //    for (int i = 0; i < m_currentSize; i++)
        //    {
        //        int j = i;
        //    
        //        while ((j > 0) && (m_entityIDs[j] < m_entityIDs[j - 1]))
        //        {
        //            Swap(j - 1, j);
        //            j--;
        //        }
        //    }
        //    Version++;
        //}
    }
}
