﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CFES
{
    public class Entity
    {
        public int ID;
        public int Mask;

        public bool CompareMask(int mask)
        {
            return (Mask & mask) == mask;
        }
    }

    public abstract class IEntitySystem
    {
        public abstract List<ComponentStorage> GetComponentStorages();
        public abstract Entity GetEntity(int entityID);
        public abstract bool TryGetEntity(int entityID, out Entity entity);
        public abstract bool EntityExists(int entityID);
        public abstract void SetComponent<T>(int entityID, T c) where T : struct, IComponent;
        public abstract void SetComponent<T>(Entity entity, T c) where T : struct, IComponent;
        public abstract bool HasComponent<T>(int entityID)where T : struct, IComponent;
        public abstract bool HasComponent<T>(Entity entity)where T : struct, IComponent;
        public abstract void RemoveComponent<T>(int entityID)where T : struct, IComponent;
        public abstract void RemoveComponent<T>(Entity entity) where T : struct, IComponent;
        public abstract T GetComponent<T>(Entity entity) where T : struct, IComponent;
        public abstract T GetComponent<T>(int entityID) where T : struct, IComponent;
        public abstract Entity CreateEntity(System.Action<Entity> initFunc);
        public abstract void DestroyEntity(int i);
        public abstract void RegisterComponentArray<T>() where T : struct, IComponent;
        public abstract ComponentStorage<T> GetComponentStorage<T>() where T : struct, IComponent;
    }

    public class ComponentEntitySystem : IEntitySystem
    {
        #region PUBLIC INTERFACE

        public override List<ComponentStorage> GetComponentStorages()
        {
            return m_arrays;
        }

        public override Entity GetEntity(int entityID)
        {
            if(!EntityExists(entityID))
                throw new Exception(string.Format("GetEntity: Entity {0} doesn't exist", entityID));

            return m_entities[entityID];
        }

        public override bool TryGetEntity(int entityID, out Entity entity)
        {
            return m_entities.TryGetValue(entityID, out entity);
        }

        public override bool EntityExists(int entityID)
        {
            return m_entities.ContainsKey(entityID);
        }

        public override void SetComponent<T>(int entityID, T c)
        {
            Entity entity;
            if(!TryGetEntity(entityID, out entity))
                throw new Exception(string.Format("SetComponent<{0}>: Entity {1} doesn't exist", typeof(T).ToString(), entityID));

            SetComponentUnsafe(entity, c);
        }

        public override void SetComponent<T>(Entity entity, T c)
        {
            SetComponent<T>(entity.ID, c);
        }

        public override bool HasComponent<T>(int entityID)
        {
            Entity entity;
            if(!TryGetEntity(entityID, out entity))
                throw new Exception(string.Format("HasComponent<{0}>: Entity {1} doesn't exist", typeof(T).ToString(), entityID));

            return HasComponent<T>(entity);
        }

        public override bool HasComponent<T>(Entity entity)
        {
            return (entity.Mask & Component<T>.Mask) == Component<T>.Mask;
        }

        public override void RemoveComponent<T>(int entityID)
        {
            Entity entity;
            if(!TryGetEntity(entityID, out entity))
                throw new Exception(string.Format("RemoveComponent<{0}>: Entity {1} doesn't exist", typeof(T).ToString(), entityID));

            if(!HasComponent<T>(entity))
                throw new Exception(string.Format("RemoveComponent<{0}>: Entity {1} doesn't have component", typeof(T).ToString(), entityID));

            RemoveComponentUnsafe<T>(entity);
        }

        public override void RemoveComponent<T>(Entity entity)
        {
            RemoveComponent<T>(entity.ID);
        }

        public override T GetComponent<T>(int entityID)
        {
            Entity entity;
            if(!TryGetEntity(entityID, out entity))
                throw new Exception(string.Format("GetComponent<{0}>: Entity {1} doesn't exist", typeof(T).ToString(), entityID));

            if(!HasComponent<T>(entity))
                throw new Exception(string.Format("GetComponent<{0}>: Entity {1} doesn't have component", typeof(T).ToString(), entityID));

            return GetComponentStorage<T>().GetComponent(entityID);
        }

        public override T GetComponent<T>(Entity entity)
        {
            return GetComponent<T>(entity.ID);
        }

        // Returns pointer to created entity
        public override Entity CreateEntity(System.Action<Entity> initFunc = null)
        {
            Entity entity = new Entity();

            entity.ID = ++m_nextEntityID;
            m_entities.Add(entity.ID, entity);

            if (initFunc != null)
                initFunc(entity);

            if (EvtEntityCreated != null)
                EvtEntityCreated(entity);

            return entity;
        }

        public override void DestroyEntity(int entityID)
        {
            Entity entity;
            if (!TryGetEntity(entityID, out entity))
                throw new Exception(string.Format("DestroyEntity: Entity {0} doesn't exist", entityID));

            foreach (var a in m_arrays)
            {
                if(entity.CompareMask(a.Mask))
                    a.RemoveComponent(entityID);
            }

            m_entities.Remove(entityID);

            if (EvtEntityDestroyed != null)
                EvtEntityDestroyed(entity);
        }

        public override void RegisterComponentArray<T>()
        {
            //UnityEngine.Debug.Log("RegisterComponentArray " + typeof(T).ToString());
            var a = new ComponentStorage<T>(true);

            // TODO: Static constructor increments the ID, will not work if there are multiple systems
            if (m_arrays.Count != Component<T>.ID)
                throw new Exception("Invalid ComponentArray ID " + typeof(T).ToString());
            m_arrays.Add(a);
        }

        public override ComponentStorage<T> GetComponentStorage<T>() 
        {
            if (Component<T>.ID >= m_arrays.Count)
            {
                if (m_autoRegisterNewTypes)
                    RegisterComponentArray<T>();
                else
                    throw new Exception("Unknown component type: " + typeof(T).ToString());
            }
            return ((ComponentStorage<T>)m_arrays[Component<T>.ID]);
        }

        #endregion

        public event System.Action<Entity> EvtEntityCreated;
        public event System.Action<Entity> EvtEntityDestroyed;

        public ComponentEntitySystem(bool autoRegisterNewTypes = true)
        {
            m_arrays = new List<ComponentStorage>();
            m_signatures = new Dictionary<int, Signature>();
            m_entities = new Dictionary<int,Entity>();
            m_autoRegisterNewTypes = autoRegisterNewTypes;
        }

        #region PRIVATE FUNCTIONS

        private void SetComponentUnsafe<T>(Entity entity, T c) where T : struct, IComponent
        {
            entity.Mask |= Component<T>.Mask;
            GetComponentStorage<T>().SetComponent(entity.ID, c);
        }

        private void RemoveComponentUnsafe<T>(Entity entity) where T : struct, IComponent
        {
            entity.Mask &= ~Component<T>.Mask;
            GetComponentStorage<T>().RemoveComponent(entity.ID);
        }

        #endregion

        private List<ComponentStorage> m_arrays;
        private Dictionary<int, Signature> m_signatures;
        private Dictionary<int, Entity> m_entities;

        private int m_nextEntityID;
        private bool m_autoRegisterNewTypes;
    }
}
