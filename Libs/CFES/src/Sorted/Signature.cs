﻿using System;
using System.Collections.Generic;

namespace CFES
{
    public class Signature
    {
        public int Mask;
        protected int[] m_maskArray;

        public Signature(int[] maskArray)
        {
            this.m_maskArray = maskArray;
        }
    }

    public class Signature<C1> : Signature where C1 : struct, IComponent
    {
        public delegate void ProcessFunc(ref C1 c1);

        private C1[] m_array1;

        public Signature(int[] maskArray, C1[] array1) : base (maskArray)
        {
            this.m_array1 = array1;
        }

    }

    public class Signature<C1, C2> : Signature  where C1 : struct, IComponent where C2 : struct, IComponent
    {
        public delegate void ProcessFunc(ref C1 c1, ref C2 c2);
        
        private C1[] m_array1;
        private C2[] m_array2;

        private List<int> m_entities;

        public Signature(int[] maskArray, C1[] array1, C2[] array2) : base (maskArray)
        {
            this.m_array1 = array1;
            this.m_array2 = array2;
            m_entities = new List<int>();
        }

        public void ForEach(ProcessFunc process)
        {
            int mask = Component<C1>.Mask | Component<C2>.Mask;

            for (int i = 0; i < m_maskArray.Length; ++i)
            {
                if ((m_maskArray[i] & mask) == mask)
                    process(ref m_array1[i], ref m_array2[i]);
            }
        }

    }
}
