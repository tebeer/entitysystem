﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CFES
{

    public class SortedEntitySystem<SystemType>
        where SystemType : SortedEntitySystem<SystemType>
    {
        #region PUBLIC INTERFACE
        
        public int ResolvePtr(EntityPtr ptr)
        {
            return m_entityPtr[ptr.ptr];
        }

        public void SetComponent<T>(EntityPtr ptr, T c) where T : struct, IComponent
        {
            m_mask[ptr.ptr] |= Component<T>.Mask;
            ((ComponentCache<T>)m_arrays[Component<T>.ID]).array[ptr.ptr] = c;
        }

        public void RemoveComponent<T>(EntityPtr ptr, T c) where T : struct, IComponent
        {
            m_mask[ptr.ptr] &= ~Component<T>.Mask;
        }

        public T GetComponent<T>(EntityPtr ptr) where T : struct, IComponent
        {
            return ((ComponentCache<T>)m_arrays[Component<T>.ID]).array[ptr.ptr];
        }

        // Returns pointer to created entity
        public EntityPtr CreateEntity(System.Action<SystemType, EntityPtr> initFunc, bool sort)
        {
            // Create entity at the end of the array
            int newEntity = m_liveEntityCount++;

            EntityPtr ptr = GetPointer(newEntity);

            if (initFunc != null)
                initFunc((SystemType)this, ptr);

            if (EvtEntityCreated != null)
                EvtEntityCreated((SystemType)this, ptr);

            if(sort)
                Sort(newEntity);

            return ptr;
        }

        public void DestroyEntity(int i)
        {
            // TODO: Find entityID
            m_mask[i] = 0;
            if (EvtEntityDestroyed != null)
                EvtEntityDestroyed((SystemType)this, i);
        }

        public void RegisterComponentArray<T>(T[] array) where T : struct, IComponent
        {
            var a = new ComponentCache<T>(array);
            UnityEngine.Debug.Log(typeof(T).ToString() + ": " + Component<T>.ID);

            if (m_arrays.Count != Component<T>.ID)
                throw new Exception("Invalid ComponentArray ID " + typeof(T).ToString());
            m_arrays.Add(a);
        }

        public bool CompareMask(int mask, int entityID)
        {
            return (m_mask[entityID] & mask) == mask;
        }

        public bool CompareMask(int mask, EntityPtr ptr)
        {
            return (m_mask[ptr.ptr] & mask) == mask;
        }

        //public void ForEach<T1, T2>(Signature<T1, T2>.ProcessFunc func) where T1 : struct, IComponent where T2 : struct, IComponent
        public Signature<T1, T2> GetSignature<T1, T2>() where T1 : struct, IComponent where T2 : struct, IComponent
        {
            int mask = Component<T1>.Mask | Component<T2>.Mask;

            Signature<T1, T2> sig;
            if (!m_signatures.ContainsKey(mask))
            {
                var array1 = (ComponentCache<T1>)m_arrays[Component<T1>.ID];
                var array2 = (ComponentCache<T2>)m_arrays[Component<T2>.ID];

                sig = new Signature<T1, T2>(m_mask, array1.array, array2.array);
                m_signatures.Add(mask, sig);
            }
            else
                sig = (Signature<T1, T2>)m_signatures[mask];

            return sig;
        }

#endregion

        public event System.Action<SystemType, EntityPtr> EvtEntityCreated;
        public event System.Action<SystemType, int> EvtEntityDestroyed;

        public readonly int EntityCount;
        public int LiveEntityCount { get { return m_liveEntityCount; } }

        public readonly int[] m_mask;
        public readonly int[] m_entityPtr;
        public readonly int[] m_entityIndex;

        private int m_liveEntityCount;

        public SortedEntitySystem(int entityCount)
        {
            EntityCount = entityCount;

            m_mask = new int[entityCount];
            m_entityPtr = new int[entityCount];
            m_entityIndex = new int[entityCount];
            for (int i = 0; i < entityCount; ++i)
            {
                m_entityPtr[i] = i;
                m_entityIndex[i] = i;
            }

            m_arrays = new List<ComponentCache>();
            m_signatures = new Dictionary<int, Signature>();
        }

        private EntityPtr GetPointer(int index)
        {
            //for (int i = 0; i < EntityCount; ++i)
            //    if (m_entityPtr[i] == index)
            //        return new EntityPtr(i);
            if(m_entityPtr[m_entityIndex[index]] != index)
                throw new Exception("Invalid pointer: " + index);
            return new EntityPtr(m_entityIndex[index]);
        }


        private void Swap(int p, int i)
        {
            int tmp = m_mask[p];
            m_mask[p] = m_mask[i];
            m_mask[i] = tmp;
            for (int j = 0; j < m_arrays.Count; ++j)
                m_arrays[j].Swap(i, p);

            // Update pointers
            tmp = m_entityIndex[p];
            m_entityIndex[p] = m_entityIndex[i];
            m_entityIndex[i] = tmp;

            m_entityPtr[m_entityIndex[p]] = p;
            m_entityPtr[m_entityIndex[i]] = i;
        }

        public void Sort(int i)
        {
            int p = Find(i);
            //UnityEngine.Debug.Log(m_liveEntityCount + " i1 " + i + " mask " + m_mask[i] + " tmpMask " + tmpMask + " p " + p);

            if (p != i)
            {
                Swap(p, i);
                // ind- 1-1 2-2 3-3 4-4 5-5 6-6 7-7 8-8 9-9
                // ptr- 1-1 2-2 3-3 4-4 5-5 6-6 7-7 8-8 9-9

                // ind- 1-1 2-2 3-3 4-5 5-4 6-6 7-7 8-8 9-9
                // ptr- 1-1 2-2 3-3 4-5 5-4 6-6 7-7 8-8 9-9

                // ind- 1-1 2-5 3-3 4-2 5-4 6-6 7-7 8-8 9-9
                // ptr- 1-1 2-4 3-3 4-5 5-2 6-6 7-7 8-8 9-9

                //m_reverseEntityPtr[p]

                Sort(i);
            }
        }

        public void InsertionSort()
        {
            for (int i = 0; i < EntityCount; i++)
            {
                int j = i;
            
                while ((j > 0) && (m_mask[j] < m_mask[j - 1]))
                {
                    Swap(j - 1, j);
                    j--;
                }
            }
        }

        private int Find(int i)
        {
            // Find the tail of entities with this mask
            int mask = m_mask[i];
            while (i > 0 && mask > m_mask[i - 1])
                --i;

            while (i < m_liveEntityCount - 1 && mask <= m_mask[i + 1])
                ++i;
            return i;
        }

        private List<ComponentCache> m_arrays;
        private Dictionary<int, Signature> m_signatures;

    }
}
