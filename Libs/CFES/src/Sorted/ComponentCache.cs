﻿
namespace CFES
{
    internal abstract class ComponentCache
    {
        public abstract void GetTemp(int i);
        public abstract void SwapTemp(int i);
        public abstract void Swap(int i1, int i2); 
    }

    internal class ComponentCache<T> : ComponentCache where T : struct, IComponent
    {
        public T[] array;
        private T tempValue;

        public ComponentCache(T[] array)
        {
            this.array = array;
        }

        public override void GetTemp(int i)
        {
            tempValue = array[i];
        }

        public override void SwapTemp(int i)
        {
            var t = tempValue;
            tempValue = array[i];
            array[i] = t;
        }

        public override void Swap(int i1, int i2)
        {
            var tmp = array[i1];
            array[i1] = array[i2];
            array[i2] = tmp;
        }
    }
}
